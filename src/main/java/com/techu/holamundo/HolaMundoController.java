package com.techu.holamundo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HolaMundoController {

    @RequestMapping(path = "/saludo", method = RequestMethod.GET)
    public String saludar(){
        return "Hola Tech U!";
    }
}

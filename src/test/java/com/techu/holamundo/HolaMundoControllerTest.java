package com.techu.holamundo;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

//@SpringBootTest
public class HolaMundoControllerTest {

    @Test
    public void testCaseHM(){
        HolaMundoController holaMundoController = new HolaMundoController();
        Assert.assertEquals("Hola equipo Tech U!", holaMundoController.saludar());
    }
}